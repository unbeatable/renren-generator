package com.gosuncn.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gosuncn.common.model.ResultVo;
import com.gosuncn.common.query.CommonQuery;
import com.gosuncn.entity.${className};
import com.gosuncn.service.I${className}Service;
import com.gosuncn.util.PageUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * ${comments}处理器
 *
 * @author ${author}
 * @email ${email}
 * @date ${datetime}
 */
@RestController
@RequestMapping("/${classname}")
public class ${className}Controller {

    @Resource
    private I${className}Service ${classname}Service;

    /**
     * 查询${comments}
     *
     * @param id
     * @return
     */
    @PostMapping("/get")
    public ResultVo<${className}> get(Long id) {
        ${className} ${classname} =${classname}Service.getById(id);
        return ResultVo.result("获取数据成功", ${classname});
    }

    /**
     * 查询${comments}
     *
     * @param commonQuery
     * @return
     */
    @PostMapping("/list")
    public ResultVo<${className}> list(@RequestBody CommonQuery commonQuery) {
        PageUtil.startPage(commonQuery);
        LambdaQueryWrapper<${className}> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.orderByDesc(${className}::getUpdatedAt);
        List<${className}> list = ${classname}Service.list(queryWrapper);
        return ResultVo.page(list);
    }

    /**
     * 插入/修改${comments}
     *
     * @param ${classname}
     * @return
     */
    @PostMapping(value = "/save")
    public ResultVo<Void> save(@RequestBody ${className} ${classname}) {
        boolean saveOrUpdate = ${classname}Service.saveOrUpdate(${classname});
        return saveOrUpdate ? ResultVo.success("保存数据成功") : ResultVo.error("保存数据失败");
    }

    /**
     * 删除${comments}
     *
     * @param id
     * @return
     */
    @PostMapping(value = "/delete")
    public ResultVo<Void> delete(Long id) {
        boolean removeById = ${classname}Service.removeById(id);
        return removeById ? ResultVo.success("删除数据成功") : ResultVo.error("删除数据失败");
    }
}
